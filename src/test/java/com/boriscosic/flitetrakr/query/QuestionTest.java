package com.boriscosic.flitetrakr.query;

import junit.framework.TestCase;


public class QuestionTest extends TestCase {
    public void isQuestionEqual(String message, Type type, String route, int number, int value, Question q) {
        assertEquals(message, type, q.getType());
        assertEquals(message, route, q.getRoute());
        assertEquals(message, number, q.getNumber());
        assertEquals(message, value, q.getValue());
    }

    public void testValidQuestion() throws Exception {
        Question question = new Question("#1: What is the price of the connection NUE-FRA-LHR?");
        isQuestionEqual("Should parse question", Type.PRICE, "NUE-FRA-LHR", 1, 0, question);

        question = new Question("#1: What is the price of the connection NUE-FRA-LHR after December 1?");
        isQuestionEqual("Should parse oversized question", Type.PRICE, "NUE-FRA-LHR", 1, 1, question);

        question = new Question("What is the price of the connection");
        isQuestionEqual("Should fail without flight path", Type.UNKNOWN, "", Integer.MAX_VALUE, 0, question);

        question = new Question("What is the price of the connection NUE-FRA-LHFFR");
        isQuestionEqual("Should fail on long airport codes", Type.UNKNOWN, "", Integer.MAX_VALUE, 0, question);

        question = new Question("What is the price of the connection NU-FA-LR");
        isQuestionEqual("Should fail on short airport codes", Type.UNKNOWN, "", Integer.MAX_VALUE, 0, question);

        question = new Question("#4: What is the cheapest connection from NUE to AMS?");
        isQuestionEqual("Should find cheapest", Type.CHEAPEST, "NUE-AMS", 4, 0, question);

        question = new Question("#7: How many with maximum 3 stops exists between NUE and FRA?");
        isQuestionEqual("Should find maximum connections", Type.MAXIMUM_CONNECTIONS, "NUE-FRA", 7, 3, question);

        question = new Question("#7: How many with minimum 3 stops exists between NUE and FRA?");
        isQuestionEqual("Should find minimum connections", Type.MINIMUM_CONNECTIONS, "NUE-FRA", 7, 3, question);

        question = new Question("#7: How many with exactly 3 stops exists between NUE and FRA?");
        isQuestionEqual("Should find exact connections", Type.EXACT_CONNECTIONS, "NUE-FRA", 7, 3, question);

        question = new Question("#9: Find all conenctions from NUE to LHR below 170Euros!");
        isQuestionEqual("Should find all connections", Type.BELOW_PRICE, "NUE-LHR", 9, 170, question);
    }
}