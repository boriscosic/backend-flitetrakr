package com.boriscosic.flitetrakr.query;


import org.omg.CORBA.UNKNOWN;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Question {
    private Type type;
    private int value;
    private String route;
    private int number;

    Question(String str) {
        String question = str.replaceAll("(?i)\\s+to\\s+|\\s+and\\s+", "-");
        setType(question);
        setValue(question);
        setRoute(question);
        setNumber(question);
    }

    /**
     * Extract any numerical values out of the query that may be needed
     */
    private void setValue(String question) {
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(question.replaceAll("#\\d+: ", ""));
        value = (matcher.find()) ? Integer.parseInt(matcher.group(0)) : 0;
    }

    /**
     * Extract and normalize route to lookup
     */
    private void setRoute(String question) {
        Pattern pattern = Pattern.compile("([A-Z]{3})(-[A-Z]{3}){1,9}(\\s|\\?$|$)");
        Matcher matcher = pattern.matcher(question);
        this.route = "";

        if (matcher.find())
            this.route = matcher.group(0).replace("?", "").trim();
    }

    /**
     * Determine type of query in order to run the proper path finding / traversal on graph
     */
    private void setType(String question) {
        type = null;
        Map<Type, String> patterns = new HashMap<Type, String>();
        patterns.put(Type.PRICE, "(?i:.*is*.*price.*([A-Z]{3}-[A-Z]{3})(\\s|\\?$|$))");
        patterns.put(Type.CHEAPEST, "(?i:.*is*.*cheapest.*([A-Z]{3}-[A-Z]{3})(\\s|\\?$|$))");
        patterns.put(Type.MAXIMUM_CONNECTIONS, "(?i:.*with*.*maximum.*\\d.*([A-Z]{3}-[A-Z]{3})(\\s|\\?$|$))");
        patterns.put(Type.MINIMUM_CONNECTIONS, "(?i:.*with*.*minimum.*\\d.*([A-Z]{3}-[A-Z]{3})(\\s|\\?$|$))");
        patterns.put(Type.EXACT_CONNECTIONS, "(?i:.*with*.*exact.*\\d.*([A-Z]{3}-[A-Z]{3})(\\s|\\?$|$))");
        patterns.put(Type.BELOW_PRICE, "(?i:.*([A-Z]{3}-[A-Z]{3})(\\s).*below.*\\d*.)");

        for(Type key : patterns.keySet()) {
            Pattern pattern = Pattern.compile(patterns.get(key));
            Matcher matcher = pattern.matcher(question);
            if (matcher.find())
                type = key;
        }

        if (type == null) type = Type.UNKNOWN;
    }

    /**
     * Extract query number for displaying with answer
     */
    private void setNumber(String question) {
        String[] parts = question.split(" ");
        String digits = parts[0].replaceAll("[^\\d]", "");
        number = digits.length() > 0 && parts.length > 0 ? Integer.parseInt(digits) : Integer.MAX_VALUE;
    }

    Type getType() {
        return type;
    }

    int getValue() {
        return value;
    }

    String getRoute() {
        return route;
    }

    int getNumber() {
        return number;
    }
}
