package com.boriscosic.flitetrakr.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    private List<String> flights;
    private List<String> questions;

    /**
     * Read and parse input file for flights and questions. Add some basic file checking to ensure correct format.
     * @param file Path of file to read in
     * @throws Exception Returns any issues found with file
     */
    public void readInFile(final String file) throws Exception {
        List<String> input = new ArrayList<String>();

        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;

        while ((line = bufferedReader.readLine()) != null)
            input.add(line);

        bufferedReader.close();

        if (input.size() < 2) {
            throw new Exception("File must contain a connection list and questions.");
        }

        setFlights(input);
        setQuestions(input);

        if (flights.size() == 0 || !input.get(0).matches("(?i:.*connection.*)")) {
            throw new Exception("No flight connections found in this file.");
        } else if (questions.size() == 0) {
            throw new Exception("No questions found in this file.");
        }
    }

    /**
     * From provided inputs generate a list of flight paths.
     */
    public void setFlights(List<String> input) {
        flights = new ArrayList<String>();
        String flights = input.get(0);
        Pattern pattern = Pattern.compile("[A-Z]{3}-[A-Z]{3}-\\d+");
        Matcher matcher = pattern.matcher(flights);
        while(matcher.find()) {
            this.flights.add(flights.substring(matcher.start(), matcher.end()));
        }
    }

    /**
     * From provided input extract questions to query against flights.
     */
    public void setQuestions(List<String> input) {
        int index = (input.get(0).matches("(?i:.*connection.*)")) ? 1 : 0;
        this.questions = input.subList(index, input.size());
    }

    /**
     * @return Array list of all flights
     */
    public List<String> getFlights() {
        return flights;
    }

    /**
     * @return Array list of questions.
     */
    public List<String> getQuestions() {
        return questions;
    }
}
