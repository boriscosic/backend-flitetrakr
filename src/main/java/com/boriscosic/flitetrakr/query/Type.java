package com.boriscosic.flitetrakr.query;


public enum Type {
    PRICE,
    CHEAPEST,
    MAXIMUM_CONNECTIONS,
    EXACT_CONNECTIONS,
    BELOW_PRICE,
    MINIMUM_CONNECTIONS,
    UNKNOWN
}
