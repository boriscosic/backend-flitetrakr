#!/bin/sh

STAGED_FILES=$(git diff --cached --name-only --diff-filter=ACM | grep ".java\{0,1\}$")

if [[ "$STAGED_FILES" = "" ]]; then
  exit 0
fi

echo "\nValidating Code:\n"

mvn checkstyle:check

if [[ "$?" -ne 0 ]]; then
  echo "\033[41mCOMMIT FAILED:\033[0m Your commit contains files that should pass Maven checkstyle but do not. Please fix the checkstyle errors and try again.\n"
  exit 1
else
  echo "\033[42mCOMMIT SUCCEEDED\033[0m\n"
fi

echo "\nRunning tests:\n"

mvn test

if [[ "$?" -ne 0 ]]; then
  echo "\033[41mCOMMIT FAILED:\033[0m Not all tests passed.\n"
  exit 1
else
  echo "\033[42mCOMMIT SUCCEEDED\033[0m\n"
fi

exit $?
