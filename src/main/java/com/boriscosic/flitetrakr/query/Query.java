package com.boriscosic.flitetrakr.query;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.boriscosic.flitetrakr.answer.Answer;
import com.boriscosic.flitetrakr.graph.Graph;
import com.boriscosic.flitetrakr.graph.Edge;


public class Query {
    private Graph graph;
    private List<String> flights;

    /**
     * Ask a query to ran against all the flight possibilities
     * @param flights Array list of flights obtained from Parser.getFlights()
     */
    public Query(final List<String> flights) {
        this.flights = flights;
    }

    public Query(final List<String> flights, final Graph graph) {
        this.graph = graph;
        this.flights = flights;
    }

    /**
     * Ask a query to determine a flight answer
     * @param str Query
     * @return String answer to flight query
     */
    public String askQuestion(String str) {
        graph = new Graph(flights);
        Question question = new Question(str);

        switch (question.getType()) {
            case PRICE:
                return Answer.format(question.getNumber(), whatIsThePriceOfConnection(question.getRoute()));

            case CHEAPEST:
                return Answer.format(question.getNumber(), whatIsTheCheapestConnection(question.getRoute()));

            case MAXIMUM_CONNECTIONS:
                return Answer.format(question.getNumber(),
                        howManyConnectionWithStops(question.getRoute(), 0, question.getValue()));

            case EXACT_CONNECTIONS:
                return Answer.format(question.getNumber(),
                        howManyConnectionWithStops(question.getRoute(), question.getValue(), question.getValue()));

            case MINIMUM_CONNECTIONS:
                return Answer.format(question.getNumber(),
                        howManyConnectionWithStops(question.getRoute(), question.getValue(), 10));

            case BELOW_PRICE:
                return Answer.format(question.getNumber(),
                        whatAreConnectionsBelowPrice(question.getRoute(), question.getValue()));

            default:
                return Answer.unknownQuestion(str);
        }
    }

    /**
     * Determine the lower price given a set of nodes
     * @param route Route to validate or price check
     * @return String with price or no connections found
     */
    public String whatIsThePriceOfConnection(final String route) {
        List<Edge> edges = graph.traverse(route);
        if (edges == null) return "No such connection found!";
        else {
            int cost = 0;
            for (Edge e: edges) cost += e.getWeight();
            return Integer.toString(cost);
        }
    }

    /**
     * Determine the cheapest connections for the route
     * @return Cheapest connection option
     */
    public String whatIsTheCheapestConnection(final String route) {
        Edge org = new Edge(route), dest = new Edge(route);
        if (dest.getStart().equals(dest.getEnd())) {
            dest = new Edge("START-END");
            graph.splitEdge(org.getStart(), dest);
        }

        List<String> optimalRoute = graph.path(dest.getPath());
        int cheapestCost = Integer.parseInt(optimalRoute.get(optimalRoute.size() - 1));

        if (cheapestCost == Integer.MAX_VALUE) {
            return "";
        } else {
            optimalRoute.set(0, org.getStart());
            optimalRoute.set(optimalRoute.size() - 1, org.getEnd());
            String result = "";
            for (String s : optimalRoute) result += s + "-";
            return result + cheapestCost;
        }
    }

    /**
     * Return a connection that satisfies a condition of min and max stop
     * @param minStops Number of minimum stops
     * @param maxStops Number of maximum stops
     * @return Number of stops with given stop parameters
     */
    public String howManyConnectionWithStops(final String route, final int minStops, final int maxStops) {
        Edge edge = new Edge(route);

        List<String> connections = graph.depth(edge.getStart(), maxStops);
        Pattern pattern = Pattern.compile(String.format("^%s(-[A-z]{3}){%d,%d}-%s",
                edge.getStart(), minStops, maxStops, edge.getEnd()));

        List<String> routes = new ArrayList<String>();
        for(String conn: connections) {
            Matcher matcher = pattern.matcher(conn);
            if (matcher.find() && routes.indexOf(matcher.group(0)) == -1) {
                routes.add(matcher.group(0));
            }
        }

        return Integer.toString(routes.size());
    }

    /**
     * Find connections below given price
     * @param price Price to search under
     * @return List all paths below given price
     */
    public String whatAreConnectionsBelowPrice(final String route, final int price) {
        Edge path = new Edge(route);
        List<String> connections = graph.depth(route, 30);
        Pattern pattern = Pattern.compile(String.format("^%s(-[A-z]{3}){%d,%d}-%s",
                path.getStart(), 0, Integer.MAX_VALUE, path.getEnd()));

        List<String> routes = new ArrayList<String>();

        for(String conn: connections) {
            Matcher matcher = pattern.matcher(conn);
            if (matcher.find()) {
                int rtPrice = Integer.parseInt(whatIsThePriceOfConnection(matcher.group(0)));
                if (rtPrice < price) {
                    String rt = String.format("%s-%d", matcher.group(0), rtPrice);
                    if (routes.indexOf(rt) == -1) routes.add(rt);
                }
            }
        }

        String result = "";
        for (String s : routes) result += s + ", ";

        return result.length() == 0 ? "" : result.substring(0, result.length() - ", ".length());
    }
}
