package com.boriscosic.flitetrakr.query;

import com.boriscosic.flitetrakr.graph.Graph;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class QueryTest extends TestCase {
    Query q;
    Graph g;
    List<String> paths;

    /*
    Rough diagram of next test case

	    /-->1-->bbb-->2-->ccc
	aaa/                   |1
	   \<--------6--------fff

	    /--2-->eee-->2-->fff
	ddd/-------->3------->/
	   \--->1-->bbb--2-->ccc
	*/
    public void setUp() throws Exception {
        super.setUp();

        paths = new ArrayList<String>();
        paths.add("aaa-bbb-1");
        paths.add("bbb-ccc-2");
        paths.add("ddd-eee-2");
        paths.add("eee-fff-2");
        paths.add("ddd-fff-3");
        paths.add("aaa-fff-5");
        paths.add("ddd-bbb-1");
        paths.add("fff-aaa-6");
        paths.add("ccc-fff-1");
    }

    public void testWhatIsThePriceOfConnection() throws Exception {
        g = new Graph(paths);
        q = new Query(paths, g);
        assertEquals("Should find cheapest connection", "3", q.whatIsThePriceOfConnection("aaa-bbb-ccc"));

        g = new Graph(paths);
        q = new Query(paths, g);
        assertEquals("Should not find cheapest connection", "No such connection found!",
                q.whatIsThePriceOfConnection("aaa-bbb-ccc-ddd"));
    }

    public void testWhatIsTheCheapestConnection() throws Exception {
        g = new Graph(paths);
        q = new Query(paths, g);
        assertEquals("Should find cheapest connection", "ddd-fff-3", q.whatIsTheCheapestConnection("ddd-fff"));

        g = new Graph(paths);
        q = new Query(paths, g);
        assertEquals("Should not find cheapest connection", "", q.whatIsTheCheapestConnection("aaa-ddd"));
    }

    public void testHowManyConnectionWithStops() throws Exception {
        g = new Graph(paths);
        q = new Query(paths, g);
        assertEquals("Should find 1 connection with stops", "1", q.howManyConnectionWithStops("aaa-fff", 0, 3));
    }

    public void testWhatAreConnectionsBelowPrice() throws Exception {
        g = new Graph(paths);
        q = new Query(paths, g);
        assertEquals("Should find no connections below price", "", q.whatAreConnectionsBelowPrice("aaa-fff", 10));
    }
}