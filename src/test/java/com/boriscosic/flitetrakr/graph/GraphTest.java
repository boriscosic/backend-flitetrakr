package com.boriscosic.flitetrakr.graph;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;


public class GraphTest extends TestCase {
    Graph g;

    /*
    Rough diagram of next test case

	    /-->1-->bbb-->2-->ccc
	aaa/                   |1
	   \<--------6--------fff

	    /--2-->eee-->2-->fff
	ddd/-------->3------->/
	   \--->1-->bbb
	*/
    public void setUp() throws Exception {
        super.setUp();

        List<String> paths = new ArrayList<String>();
        paths.add("aaa-bbb-1");
        paths.add("bbb-ccc-2");
        paths.add("ddd-eee-2");
        paths.add("eee-fff-2");
        paths.add("ddd-fff-3");
        paths.add("aaa-fff-5");
        paths.add("ddd-bbb-1");
        paths.add("fff-aaa-6");
        paths.add("ccc-fff-1");
        g = new Graph(paths);
    }

    public void testFindEdge() throws Exception {
        Edge e = g.findEdge("aaa-bbb");
        assertEquals("Should find an edge by path", "aaa-bbb", e.getPath());

        e = g.findEdge("a-c");
        assertEquals("Should be a null edge", null, e);
    }

    public void testFindNode() throws Exception {
        Node n = g.findNode("aaa");
        assertEquals("Should find node by name", "aaa", n.getName());

        n = g.findNode("d");
        assertEquals("Should be a null node", null, n);
    }

    public void testTraverse() throws Exception {
        List<Edge> foundEdges = new ArrayList<Edge>();
        foundEdges.add(g.findEdge("aaa-bbb"));
        assertNotSame("Should not find the traverse", foundEdges, g.traverse("aaa-bbb-ccc"));

        foundEdges.add(g.findEdge("bbb-ccc"));
        assertEquals("Should find the traverse", foundEdges, g.traverse("aaa-bbb-ccc"));
    }

    public void testPath() throws Exception {
        assertEquals("Should find path", "bbb-ccc", g.findEdge("bbb-ccc").getPath());
        assertEquals("Should be a null path", null, g.findEdge("aaa-ddd"));
    }

    public void testSplitEdge() throws Exception {
        g.splitEdge("aaa", new Edge("START-END"));
        assertEquals("Should find split start node", "START", g.findNode("START").getName());
        assertEquals("Should find split end node", "END", g.findNode("END").getName());
        assertEquals("Should find start edge", "START-bbb", g.findEdge("START-bbb").getPath());
        assertEquals("Should find end edge", "fff-END", g.findEdge("fff-END").getPath());
    }
}