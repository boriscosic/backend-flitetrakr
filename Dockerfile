FROM openjdk:6
RUN apt-get update && apt-get install -y maven
ENV WD /opt/flitetrakr

WORKDIR $WD
COPY . $WD
RUN mvn package


CMD ["java", "-jar", "target/backend-flitetrakr-1.0.jar"]
