# FliteTrakr

This is a Java utility for a programming assignment. Given a set of flight paths and their amounts this utility can
calculate best paths, cheapest price and flight paths. See below for instructions on building and running the
application.

Problem: https://bitbucket.org/adigsd/backend-flitetrakr

## Requirements

This app requires Java JRE 1.6+ and Maven. If Java is not installed on the system the app can also be started via Docker.

## Getting Started

### Docker

This document assumes Docker is installed and running on the machine. Docker can be installed on a number of platforms by following [instructions](https://docs.docker.com/engine/installation/) on their websites. Before running Docker, ensure the folder `node_modules` is removed. `node_modules` are not necessarily universally compatible and certain tool or utilities may have platform specific requirements. `$PWD` should be a path where data files reside.

```
$ docker build -t flitetrakr .
$ docker run -v $PWD:/opt/data flitetrakr java -jar /opt/flitetrakr/target/backend-flitetrakr-1.0.jar /opt/data/input.txt
```

### Locally

After confirming the requirements for development, follow the steps below to get underway:

```
$ https://bitbucket.org/boriscosic/backend-flitetrakr
$ cd backend-flitetrakr
$ mvn package
```

Once the app has finished build it can be executed by running:

```
$ java -jar target/backend-flitetrakr-1.0.jar input.txt
```

### Contributing

Before making contributions it is recommended to connect to pre-commit hook to test and lint the application:

```
ln -s "$(pwd)/hooks/pre-commit.sh" .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
```

These steps can also be run manually:

```
$ mvn test
$ mvn checkstyle:check
```

## Appendix

This app is build on Java JRE 1.6 and Maven. JRE 1.8 lambdas would help in parsing and evaluation question types but they 
are not as timeless as regular expressions. When depending on a client platform the goal is to support one or two 
backwards reason unless a compelling reason for upgrade can be provided.

### Demo?

Due to nature of this application there is no demo. If there are any issues with the build, contact me at boriscosic@gmail.com, or utilize the Docker build instructions.
