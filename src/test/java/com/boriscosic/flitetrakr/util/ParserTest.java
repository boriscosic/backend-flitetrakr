package com.boriscosic.flitetrakr.util;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParserTest extends TestCase {
    private Parser parser;
    private ClassLoader classLoader;

    public void setUp() throws Exception {
        super.setUp();
        parser = new Parser();
        classLoader = getClass().getClassLoader();
    }

    public void testReadInFile() throws Exception {
        parser.readInFile(classLoader.getResource("valid.txt").getFile());
        assertEquals("Parser should find flights in the connection", 5, parser.getFlights().size());
        assertEquals("Should find questions in the file", 9, parser.getQuestions().size());

        try {
            parser.readInFile(classLoader.getResource("empty.txt").getFile());
        } catch (Exception e) {
            assertEquals("Should be missing everything",
                    "File must contain a connection list and questions.", e.getMessage());
        }

        try {
            parser.readInFile(classLoader.getResource("missing_first_line.txt").getFile());
        } catch (Exception e) {
            assertEquals("Should be missing flights",
                    "No flight connections found in this file.", e.getMessage());
        }

        try {
            parser.readInFile(classLoader.getResource("missing_flights.txt").getFile());
        } catch (Exception e) {
            assertEquals("Should be missing flights", "No flight connections found in this file.", e.getMessage());
        }

        try {
            parser.readInFile(classLoader.getResource("missing_questions.txt").getFile());
        } catch (Exception e) {
            assertEquals("Should be missing questions",
                    "File must contain a connection list and questions.", e.getMessage());
        }
    }

    public void testGetFlights() throws Exception {
        parser.readInFile(classLoader.getResource("input.txt").getFile());
        List<String> routes = parser.getFlights();
        assertEquals(5, routes.size());
        assertEquals("Should find flights",
                routes, Arrays.asList("NUE-FRA-43", "NUE-AMS-67", "FRA-AMS-17", "FRA-LHR-27", "LHR-NUE-23"));
    }

    public void testGetQuestions() throws Exception {
        parser.readInFile(classLoader.getResource("input.txt").getFile());
        List<String> questions = parser.getQuestions();
        assertEquals("Should find questions", 9, questions.size());
    }

    public void testSetQuestions() throws Exception {
        List<String> questions = new ArrayList<String>();
        questions.add("Hello world");
        questions.add("Query");
        parser.setQuestions(questions);
        assertEquals("Should have added questions", 2, parser.getQuestions().size());

        questions = new ArrayList<String>();
        questions.add("connection: Hello world");
        questions.add("Query");
        parser.setQuestions(questions);
        assertEquals("Should have added a question", 1, parser.getQuestions().size());
    }

    public void testSetFlights() throws Exception {
        List<String> flights = new ArrayList<String>();
        flights.add("Connection: AAA-BBB-12, ASC-DFG-33");
        parser.setFlights(flights);
        assertEquals("Should contain flights", Arrays.asList("AAA-BBB-12", "ASC-DFG-33"), parser.getFlights());

        flights = new ArrayList<String>();
        flights.add("Connection: AA-BBB-12, ASC-FG-33, ASD-ASD");
        parser.setFlights(flights);
        assertEquals("Should not contain flights", Arrays.asList(), parser.getFlights());

        flights = new ArrayList<String>();
        flights.add("AAA-BBB-12, ASC-DFG-33");
        parser.setFlights(flights);
        assertEquals("Should set flights without connection string",
                Arrays.asList("AAA-BBB-12", "ASC-DFG-33"), parser.getFlights());
    }
}