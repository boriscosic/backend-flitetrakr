package com.boriscosic.flitetrakr.answer;

import junit.framework.TestCase;

public class AnswerTest extends TestCase {
    public void testFormat() throws Exception {
        assertEquals("Should be valid format", "#1: This is a test", Answer.format(1, "This is a test"));
        assertNotSame("Should be invalid format", "#1: This is a test", Answer.format(2, "This is a test"));
        assertEquals("Should be no connections", "#2: No such connection found!", Answer.format(2, ""));
    }
}