package com.boriscosic.flitetrakr;

import com.boriscosic.flitetrakr.query.Query;
import com.boriscosic.flitetrakr.util.Parser;

/**
 * FlightTrakr application to determine flight prices and routes.
 */
public class App {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("usage: [java -jar app.jar input.txt]");
            System.exit(0);
        }

        Parser parser = new Parser();
        try {
            parser.readInFile(args[0]);
        } catch(Exception e) {
            System.out.println(e.getMessage());
            System.exit(-1);
        }

        Query query = new Query(parser.getFlights());
        for (String line : parser.getQuestions())
            if (line.length() > 0)
                System.out.println(query.askQuestion(line));
    }
}
