package com.boriscosic.flitetrakr.answer;

/**
 * Class for formatting answers to questions.
 */
public final class Answer {

    /**
     * Generic format for each answer.
     * @param number Query number
     * @param answer Answer data
     * @return Formatted answer string
     */
    public static String format(final Integer number, final String answer) {
        if (answer.equals("")) {
            return String.format("#%s: No such connection found!", number);
        } else if(number == Integer.MIN_VALUE) {
            return String.format("%s", number, answer);
        }
        return String.format("#%s: %s", number, answer);
    }

    /**
     * Return an answer for an unknown query.
     * @param question Query for response
     * @return Formatted answer string
     */
    public static String unknownQuestion(final String question) {
        return String.format("I don't understand query: %s", question);
    }
}
