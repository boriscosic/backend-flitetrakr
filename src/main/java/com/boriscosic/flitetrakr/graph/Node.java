package com.boriscosic.flitetrakr.graph;

class Node {
    private String name;
    private boolean traveled;

    /**
     * @param name Name of the new node
     */
    Node(final String name) {
        this.name = name;
        traveled = false;
    }

    /**
     * @return Check if node has been traveled
     */
    boolean isTraveled() {
        return traveled;
    }

    /**
     * @param traveled Set node traveled status
     */
    void setTraveled(final Boolean traveled) {
        this.traveled = traveled;
    }

    /**
     * @return Get node name
     */
    String getName() {
        return name;
    }
}
