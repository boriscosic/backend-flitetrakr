package com.boriscosic.flitetrakr.graph;

public class Edge {
    private String start;
    private String end;
    private String path;
    private int weight;

    /**
     * @param path Path from one vertex to another, may include weight
     */
    public Edge(final String path) {
        this(path, 0);
        String[] edge = path.split("-");
        int weight = edge.length == 3 ? Integer.parseInt(edge[2]) : 0;
        this.weight = weight;
    }

    /**
     * @param path Path from one vertex to another, should pass weight separately
     * @param weight Weight of connecting edge
     */
    public Edge(final String path, final int weight) {
        String[] edges = path.split("-");
        start = edges[0];
        end = edges[1];
        this.path = start + "-" + end;
        this.weight = weight;
    }

    /**
     * @return Weight of edge
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @return Path from start to end
     */
    public String getPath() {
        return path;
    }

    /**
     * @return Starting node name
     */
    public String getStart() {
        return start;
    }

    /**
     * @return Ending node name
     */
    public String getEnd() {
        return end;
    }

}
