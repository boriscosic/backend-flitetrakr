package com.boriscosic.flitetrakr.graph;

import java.util.*;

/**
 * Graph operation for determining best, cheapest or viable flight.
 */
public class Graph {
    private List<Node> nodes;
    private List<Edge> edges;
    private HashMap<String, Integer> distance;
    private HashMap<String, String> previous;
    private ArrayList<String> traversed = new ArrayList<String>();

    /**
     * Initialize each graph from a list of flight routes
     * @param flights XXX-YYY-123 where XXX is the source, YYY is the dest and 123 is the price
     */
    public Graph(final List<String> flights) {
        nodes = new ArrayList<Node>();
        edges = new ArrayList<Edge>();

        for(String flight : flights) addPath(flight);
    }

    /**
     * Add an edge to the graph.
     * @param path XXX-YYY, see Graph description for full format.
     * @param weight Price of the flight or weight.
     */
    private void addEdge(final String path, final int weight) {
        if (findEdge(path) == null) edges.add(new Edge(path, weight));
    }

    /**
     * Add a node to the graph
     * @param name Name of the destination or start (XXX or YYY)
     */
    private void addNode(final String name) {
        if (findNode(name) == null) nodes.add(new Node(name));
    }

    /**
     * Find an edge in graph by path
     * @param path XXX-YYY path to the edge
     * @return Edge containing the search path or null if not found
     */
    public Edge findEdge(final String path) {
        for (Edge e : edges)
            if (e.getPath().equals(path)) return e;

        return null;
    }

    /**
     * Find a node in graph by name
     * @param name Node name, XXX or YYY
     * @return Node object containing the name
     */
    public Node findNode(final String name) {
        for (Node n: nodes)
            if (n.getName().equals(name)) return n;

        return null;
    }

    /**
     * Parse node and edge from a path and add it to the graph
     * @param path String in XXX-YYY-123 format
     */
    public void addPath(final String path) {
        Edge edge = new Edge(path);
        addNode(edge.getStart());
        addNode(edge.getEnd());
        addEdge(edge.getPath(), edge.getWeight());
    }

    /**
     * Traverse a given set of edges to determine cost
     * @param route String in format XXX-YYY-ZZZ-AAA-BBB
     * @return List of edges found or empty list.
     */
    public List<Edge> traverse(final String route) {
        List<Edge> foundEdges = new ArrayList<Edge>();
        String[] routes = route.split("-");

        for (int i = 0; i < routes.length; i++) {
            if (i == routes.length - 1) break;
            Edge edge = findEdge(String.format("%s-%s", routes[i], routes[i+1]));
            if (edge == null) return null;
            foundEdges.add(edge);
        }

        return foundEdges;
    }

    /**
     * Dijkstra algorithm for finding the shortest path. This method
     * does not return anything, it runs further to optimize the result set.
     * @param route String in format XXX-YYY
     */
    public List<String> path(final String route) {
        String[] path = route.split("-");
        distance = new HashMap<String, Integer>();
        previous = new HashMap<String, String>();

        for(Node n: nodes) {
            distance.put(n.getName(), Integer.MAX_VALUE);
            previous.put(n.getName(), null);
        }

        distance.put(path[0], 0);
        List<String> quest = new ArrayList<String>();
        quest.add(path[0]);

        while (quest.size() > 0) {
            Node n = findMinNode();
            quest.remove(n.getName());
            List<Edge> neighbours = findNeighbours(n);

            for (Edge e : neighbours) {
                int nbWeight = e.getWeight() + distance.get(n.getName());

                Node neighbour = findNode(e.getEnd());
                if (neighbour != null && quest.indexOf(neighbour.getName()) == -1 && !neighbour.isTraveled())
                    quest.add(neighbour.getName());

                if (neighbour != null)
                    if ((distance.get(neighbour.getName()) > nbWeight)) {
                        distance.put(neighbour.getName(), nbWeight);
                        previous.put(neighbour.getName(), n.getName());
                    }
            }
        }

        return findRoute(path[1]);
    }

    /**
     * Search all connecting nodes to determine viable paths
     * @param route String in format XXX-YYY
     * @param iterations How many times to iterate
     * @return Return list of paths that were iterated.
     */
    public List<String> depth(final String route, final int iterations) {
        String[] start = route.split("-");
        traversed = new ArrayList<String>();
        Queue<String> queue = new LinkedList<String>();
        String path = "";
        queue.add(start[0]);

        iterate(queue, path, (iterations * edges.size()));
        return traversed;
    }

    /**
     * Greedy function that search in circles until iterations are exceeded or
     * no neighboors are found.
     * @param queue Queue of node names to visit
     * @param path Path visit so far
     * @param iterations Maximum number of iterations
     */
    private boolean iterate(Queue<String> queue, String path, int iterations) {
        String node = queue.remove();
        path = path + node + "-";

        List<Edge> neighbours = findNeighbours(findNode(node));
        if (path.split("-").length > iterations) {
            traversed.add(path);
            return true;
        } else {
            for(Edge e: neighbours) {
                Node neighbour = findNode(e.getEnd());
                if (queue.size() == 0 ||
                    neighbour != null && !queue.contains(neighbour.getName())) queue.add(e.getEnd());
                return iterate(queue, path, iterations);
            }
        }

        return true;
    }

    /**
     * Look at connecting neighbours of a node and return them in a list
     * @param node Node to look at
     * @return List of edges that lead away from the node
     */
    private List<Edge> findNeighbours(final Node node) {
        List<Edge> neighbours = new ArrayList<Edge>();
        for (Edge e : edges) {
            if (e.getStart().equals(node.getName()))
                neighbours.add(e);
        }
        return neighbours;
    }

    /**
     * Find the minimum path node for Dijkstra
     * @return Node with minimum distance
     */
    private Node findMinNode() {
        Node minNode = null;
        int minVal = Integer.MAX_VALUE;

        for (Object o : distance.entrySet()) {
            Map.Entry d = (Map.Entry) o;
            Node n = findNode(d.getKey().toString());
            if ((n != null) && (Integer.parseInt(d.getValue().toString()) <= minVal) && !n.isTraveled()) {
                minVal = Integer.parseInt(d.getValue().toString());
                minNode = n;
            }
        }

        if (minNode != null) minNode.setTraveled(true);
        return minNode;
    }

    /**
     * Determine the cost and shortest route by reversing visited routes.
     * @param target String containing target to trace back from
     */
    private List<String> findRoute(final String target) {
        List <String> route = new ArrayList<String>();
        String last = previous.get(target);
        while (last != null) {
            route.add(last);
            last = previous.get(last);
        }
        Collections.reverse(route);
        route.add(distance.get(target).toString());
        return route;
    }

    /**
     * Create split edges and vertices when start is the same as end.
     * @param node Node name that has the same start and end
     * @param dest Edge containing new path when start is the same as end
     */
    public void splitEdge(final String node, final Edge dest) {
        addNode(dest.getStart());
        addNode(dest.getEnd());
        List<Edge> newEdges = new ArrayList<Edge>();

        for (Edge e: edges) {
            if (e.getStart().equals(node))
                newEdges.add(new Edge(dest.getStart() + "-" + e.getEnd(), e.getWeight()));
            else if (e.getEnd().equals(node))
                newEdges.add(new Edge(e.getStart() + "-" + dest.getEnd(), e.getWeight()));
        }

        for (Edge e: newEdges)  edges.add(e);
    }
}
